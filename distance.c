//WAP to find the distance between two point using 4 functions.

#include<stdio.h>
#include<math.h>

float input()
{
    float temp;
    scanf("%f",&temp);
    return temp;
}
float calc_dist(float a,float b)
{
    float result;
    result=sqrt(a*a+b*b);
    return result;
}
void show_output(float x,float y,float output)
{
    printf("distance between the points %f and %f is %2f",x,y,output);
}

int main()
{
    float x1,x2,y1,y2,x,y,dist;
    printf("for first value:\n");
    printf("enter the value for X:\n");
    x1=input();
    printf("enter the value for Y:\n");
    y1=input();

    printf("for second value:\n");
    printf("enter the value for X:\n");
    x2=input();
    printf("enter the value for Y:\n");
    y2=input();

    x=(x2-x1);
    y=(y2-y1);

    dist =calc_dist(x,y);
    show_output(x,y,dist);

    return 0;
}
