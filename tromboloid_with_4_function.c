#include<stdio.h>
float input()
{
    float temp;
    scanf("%f",&temp);
    return temp;
}
float calc_vol(float h,float b,float d)
{
    return ((h*b*d)+(d/b))/3;
}
void show_output(float h,float b,float d,float volume)
{
    printf("the volume of tromboloid when height %f,breadth %f and depth %f is:%2f\n",h,b,d,volume);
}
int main()
{
    float h,b,d,vol;
    printf("enter the height of tromboloid:\n");
    h=input();

    printf("enter the breadth of tromboloid:\n");
    b=input();
   printf("enter the depth of tromboloid:\n");
   d=input();

   vol=calc_vol(h,b,d);

   show_output(h,b,d,vol);

   return 0;
}
